# Ignoring Changes and Creating a Merged Document
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

An example pipeline showing how to post-process a delta result to 'ignore' certain changes.

This document describes how to run the sample. For concept details see: [Ignoring Changes and Creating a Merged Document](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/ignoring-changes-and-creating-a-merged-document)

If you have Ant installed, use the build script provided to run the Pipelined Comparator and Document Comparator samples. Simply type the following command to run the pipeline and produce the output files *result.xml* and *document/result.xml*.

	ant run
	
If you don't have Ant installed, you can still run each sample individually from the command-line, this is covered below.

## Pipelined Comparator
If you have Ant installed, use the build script provided to run the sample. Simply type the following command to run the pipeline and produce the output file result.xml.

	ant run-dxp
	
If you don't have Ant installed, you can run the sample from a command line by issuing the following command from the sample directory (ensuring that you use the correct directory separators for your operating system). Replace x.y.z with the major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar

	java -jar ../../deltaxml-x.y.z.jar compare ignore documentA.xml documentB.xml result.xml
	
## Document Comparator
If you have Ant installed, use the build script provided to run the sample. Simply type the following command to run the pipeline and produce the output file document/result.xml.

	ant run-dc
	
If you don't have Ant installed, you can run the sample from a command line by issuing the following command from the sample directory (ensuring that you use the correct directory and class path separators for your operating system), replacing x.y.z with the major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar:

	mkdir bin
	javac -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar -d bin ./src/java/com/deltaxml/samples/IgnoreChangesSample.java
	java -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../icu4j.jar:../../resolver.jar:./bin/com/deltaxml/samples/ com.deltaxml.samples.IgnoreChangesSample document/documentA.xml document/documentB.xml document/result.xml

To clean up the sample directory, run the following Ant command.

	ant clean
