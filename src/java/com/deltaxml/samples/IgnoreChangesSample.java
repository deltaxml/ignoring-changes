// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;

import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.DocumentComparator.ExtensionPoint;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStep;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.config.ModifiedWhitespaceBehaviour;
import com.deltaxml.cores9api.config.ResultReadabilityOptions;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.cores9api.config.PresetPreservationMode;
import net.sf.saxon.s9api.Serializer;

public class IgnoreChangesSample {
  
  public static void main(String[] args) throws Exception {
    if (args.length < 3) {
      System.out.println("Usage: IgnoreChangesSample <XML input file 1> <XML input file 2> <result file>");
      throw new Exception("Command line arguments missing");
    }
    String inputFileName1= args[0];
    String inputFileName2= args[1];
    String outputFileName= args[2];
    
    File input1= new File(inputFileName1);
    File input2= new File(inputFileName2);
    
    DocumentComparator comparator = new DocumentComparator();
    comparator.setOutputProperty(Serializer.Property.INDENT, "yes");
    
    // By default word by word comparison is performed on all elements
    ResultReadabilityOptions resultReadabilityOptions = comparator.getResultReadabilityOptions();

    // Orphaned word processing
    resultReadabilityOptions.setOrphanedWordDetectionEnabled(false);

    resultReadabilityOptions.setModifiedWhitespaceBehaviour(ModifiedWhitespaceBehaviour.NORMALIZE);
    resultReadabilityOptions.setElementSplittingEnabled(false);
    
    LexicalPreservationConfig lexConfig= new LexicalPreservationConfig(PresetPreservationMode.ROUND_TRIP);
    lexConfig.setPreserveIgnorableWhitespace(false);
    comparator.setLexicalPreservationConfig(lexConfig);
    
    FilterStepHelper fsHelper= comparator.newFilterStepHelper();
    
    FilterChain fc= fsHelper.newSingleStepFilterChain(new File("document/disable-word-by-word.xsl"), "disable-word-by-word");
    comparator.setExtensionPoint(ExtensionPoint.PRE_FLATTENING, fc);

    // Create a filter chain
    FilterChain fcIgnoreChanges= fsHelper.newFilterChain();
    // Create a filter step for an xsl file, and add it to the filter chain.
    FilterStep fsMark= fsHelper.newFilterStep(new File("document/mark-ignore-changes.xsl"), "mark-ignore-changes");
    fcIgnoreChanges.addStep(fsMark);
    // Create two filters from provided resources, adding each to the same filter chain.
    FilterStep fsApply= fsHelper.newFilterStepFromResource("/xsl/apply-ignore-changes.xsl","apply-ignore-changes");
    fcIgnoreChanges.addStep(fsApply);
    FilterStep fsPropagate= fsHelper.newFilterStepFromResource("/xsl/propagate-ignore-changes.xsl","propagate-ignore-changes");
    fcIgnoreChanges.addStep(fsPropagate);
    // Add the chain comprising of the three filters to the OUTPUT_FINAL extension point.
    comparator.setExtensionPoint(ExtensionPoint.OUTPUT_FINAL, fcIgnoreChanges);
        
    comparator.compare(input1, input2, new File(outputFileName));
  }
  
}
